package org.cuatrovientos.ed;

import java.util.ArrayList;

public class Blog {
	

		private ArrayList<Post> posts;
		
		public Blog() {
			posts = new ArrayList<Post>();
		}

		public Blog(ArrayList<Post> posts) {
			this.posts = posts;
		}
		
		public Post getPost(int index){
			if (posts.size() > index) {
				return posts.get(index);
			}
			return posts.get(0);
		}
		
		public void addPost(Post post) {
			posts.add(post);
		}

		public void removePost(int index) {
			if (posts.size() > index) {
				posts.remove(index);
			}
		}
		
		public ArrayList<Post> getPosts() {
			return posts;
		}

		public void setPosts(ArrayList<Post> posts) {
			this.posts = posts;
		}

		
		
		public String toString() {
			String msg = "";
			for (int i = 0; i < posts.size(); i++) {
				msg += posts.get(i).showPost() + "\n\n";
			}
			return msg;
		}
		
		
		
	}

	














