package org.cuatrovientos.ed;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		Blog blog = new Blog();
		String opcion;
		do {
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"); //Limpieza cutre de consola
			System.out.println("================");
				System.out.println("===== Blog =====");
				System.out.println("================ \n");
				
				System.out.println("1. Crear un post");
				System.out.println("2. Borrar un post");
				System.out.println("3. Ver Post");
				System.out.println("4. Mostrar todos los posts");
				System.out.println("5. Salir");		
				
				System.out.print("Selecciona una opci�n: ");
				opcion = scan.nextLine();
				System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"); //Saltos de linea para "limpiar"
				
				String indexStr;
				int index;
				if (opcion.equals("1")) {
					System.out.print("Introduce un titulo para el post: ");
					String titulo = scan.nextLine();
					System.out.print("Introduce un autor para el post: ");
					String autor = scan.nextLine();
					System.out.print("Introduce una fecha para el post (XX/XX/XXXX): ");
					String fecha = scan.nextLine();
					System.out.print("Introduce un tema para el post: ");
					String tema = scan.nextLine();
					
					Post post = new Post(titulo,autor,fecha,tema);
					blog.addPost(post);
				}else if (opcion.equals("2")) {
					System.out.print("Introduce el n�mero del post que quieres borrar: ");
					indexStr = scan.nextLine();
					index = Integer.parseInt(indexStr);
					
					blog.removePost(index);
				}else if (opcion.equals("3")) {
					System.out.print("Introduce el n�mero del post que quieres ver: ");
					indexStr = scan.nextLine();
					index = Integer.parseInt(indexStr);
					System.out.println(blog.getPost(index).showPost());
					System.out.println("\nPresiona Enter para continuar...");
					scan.next();
				}else if (opcion.equals("4")) {
					
					System.out.println(blog.toString());
					System.out.println("\nPresiona Enter para continuar...");
					scan.next();
				}
			}while (!opcion.equals("5"));
			
		}
	}
