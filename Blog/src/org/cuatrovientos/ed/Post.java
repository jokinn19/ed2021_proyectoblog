package org.cuatrovientos.ed;


public class Post {
	
	private String Titulo;
	private String Autor;
	private String Fecha;
	private String Tema;
	
	
	public Post(String titulo, String autor, String fecha, String tema) {
		Titulo = titulo;
		Autor = autor;
		Fecha = fecha;
		Tema = tema;
	}



	public Post() {
		
	}



	public String getTitulo() {
		return Titulo;
	}



	public void setTitulo(String titulo) {
		Titulo = titulo;
	}



	public String getAutor() {
		return Autor;
	}



	public void setAutor(String autor) {
		Autor = autor;
	}



	public String getFecha() {
		return Fecha;
	}



	public void setFecha(String fecha) {
		Fecha = fecha;
	}

	
	public String getTema() {
		return Tema;
	}


	public void setTema(String tema) {
		Tema = tema;
	}

	
	public String showPost() {			
		return "Post [Titulo=" + Titulo + ", Autor=" + Autor + ", Fecha=" + Fecha + "Tema="+ Tema + "]";
	}
	
	




}

